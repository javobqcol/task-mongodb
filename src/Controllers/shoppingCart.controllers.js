import { isValidObjectId } from 'mongoose'
import ShoppingCart from '../models/ShoppingCart'
import { handleError } from "../helper/handleError";
export const renderShoppingCart = async ( req, res ) =>{
    try {
        const shoppingCart = await ShoppingCart.find()
            .populate( 'client', 'name person_type document_type num_document' )
            .populate( 'shoppings.product', 'name presentation quantity s_price' )
        res.json( shoppingCart )   
    //    res.render("index", { tasks: tasks })

    } catch ( error ) {
    handleError( req, res, error )   
}
}

export const createShoppingCart = async ( req, res ) =>{ 
    try {
        const shoppingCart = ShoppingCart( req.body ) 
        const savedshopping = await shoppingCart.save()
        res.json( { msg:"item carrito salvado salvado" } )
            
    } catch ( error ) {
        handleError( req, res, error )   
    }
}

export const rendershoppingCartEdit = async ( req, res ) =>{ 
    try {
        const { id } = req.params
        if ( ! isValidObjectId(id) ){ // el id no es valido.. a la mierda
            res.status( 404 ).json( { msg: 'id no valido' } )
            return
        }   
        const shoppingCartExist = await ShoppingCart.findById( { _id: id } )
                                                        .populate( 'client', 'name person_type document_type num_document' )
                                                        .populate( 'shoppings.product', 'name presentation quantity s_price' )
        if( ! shoppingCartExist ){
            res.status( 404 ).json( { msg: 'no existe el Carrito de compra que se va a renderizar'} );
            return
        }  
        res.json( shoppingCartExist )
    } catch ( error ) {
        handleError( req, res, error )   
    }
}


export const editShoppingCart = async ( req, res ) =>{    
    try {
        const { id } = req.params
        if (! isValidObjectId( id ) ){ // el id no es valido.. a la mierda
            res.status( 404 ).json( {msg: 'id no valido'} );
            return
        } 
        const shoppingCartExist = await ShoppingCart.findById( { _id: id } );
        if( ! shoppingCartExist ){
            res.status( 404 ).json( {msg: 'no existe el carrito de compras que se va a actualizar' } );
            return
        }
        const newShoppingCart = Object.assign( {}, req.body )
        const shoppingCart = await ShoppingCart.findByIdAndUpdate( id, newShoppingCart )
        res.json( { msg:'Shopping Cart Actualizado' } )
        res.json( shoppingCart )
    } catch ( error ) {
        handleError( req, res, error )   
    }

}

export const deleteShoppingCart = async ( req, res ) =>{    
    try {
        const { id } = req.params
        if ( ! isValidObjectId( id ) ){ // el id no es valido.. a la mierda
            res.status( 404 ).json( { msg: 'id no valido' } );
            return
        }  
        const shoppingCartExist = await ShoppingCart.findById( { _id: id } );
        if( ! shoppingCartExist ){
            res.status( 404 ).json( { msg: 'no existe el Carrito de compras que se va a borrar' } );
            return
        } 
        const delShoppingCart = await ShoppingCart.findByIdAndDelete( {_id: id } )   
        res.json( { msg:'Shopping Cart Borrado' } )
        res.json( delShoppingCart )
    } catch ( error ) {
        handleError( req, res, error )   
    }
}
