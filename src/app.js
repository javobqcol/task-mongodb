import express, { application } from "express";
import { create } from "express-handlebars";
import userRoutes from "./routes/user.routes";
import loginRoutes from "./routes/login.routes";
import productRoutes from "./routes/product.routes";
import clientRoutes from "./routes/client.routes";
import shoppingCartRoutes from "./routes/shoppingCart.routes";

import path from "path";
import morgan from "morgan";
import cors from "cors";
import ShoppingCart from "./models/ShoppingCart";

const app = express();

// app.set("views", path.join(__dirname, "views"));
// console.log(app.get("views"));
// const exphbs = create({
//   extname: ".hbs",
//   layoutsDir: path.join(app.get("views"), "layouts"),
//   partialsDir: path.join(app.get("views"), "partials"),
//   defaultLayout: "main",
// });
// app.engine(".hbs", exphbs.engine);
// app.set("view engine", ".hbs");
 
//middleware
app.use(morgan("dev"));
app.use(express.urlencoded({ extended: false }));
app.use(cors());
app.use(express.json());

//routes
app.use(userRoutes);
app.use(loginRoutes);
app.use(productRoutes);
app.use(clientRoutes);
app.use(shoppingCartRoutes);
//static files
app.use(express.static(path.join(__dirname, "public")));

export default app;
