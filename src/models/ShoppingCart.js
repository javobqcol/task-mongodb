import { Schema, model } from "mongoose";
import Product from "./Product";

const shoppingCartSchema = new Schema(
  {
    client: {
      type: Schema.Types.ObjectId,
      ref: "client",
      required: true,
    },
    shoppings: [
      {
        product: {
          type: Schema.Types.ObjectId,
          ref: "product",
          required: true,
        },
        quantity: {
          type: Number,
        },
        value: {
          type: Number,
        },
        total_price: {
          type: Number,
        },
      },
    ],
    sum_total_price: {
      type: Number,
    },
  },
  {
    timestamps: true,
    versionKey: false,
  }
);

export default model("shoppingCart", shoppingCartSchema);
