import { Schema, model } from "mongoose";
import isEmail from "validator/lib/isEmail";

const clientSchema = new Schema(
  {
    name: {
      type: String,
      required: true,
      trim: true,
    },
    person_type: {
      //persona natural o juridica
      type: String,
      required: true,
      enum: ["Natural", `Juridica`],
      trim: true,
    },
    document_type: {
      type: String,
      required: true,
      trim: true,
      enum: [
        "Cedula",
        "NIT",
        "Cedula Extranjeria",
        "Tarjeta de identidad",
        "Pasaporte",
      ],
      default: "NIT",
    },
    num_document: {
      type: String,
      required: true,
    },
    country: {
      type: String,
    },
    state: {
      type: String,
    },
    city: {
      type: String,
    },
    email: {
      type: String,
      trim: true,
      lowercase: true,
      unique: true,
      required: "Email address is required",
      validate: [isEmail, "Please fill a valid email address"],
    },
    address: {
      type: String,
    },
    phone: {
      type: String,
    },
    delivery_address: {
      type: String,
    },
    active: {
      type: Boolean,
      default: true,
    },
  },
  {
    timestamps: true,
    versionKey: false,
  }
);

export default model("client", clientSchema);
