import { Schema, model } from "mongoose";

const productSchema = new Schema(
  {
    idcode: {
      type: String,
      required: true,
      unique: true,
      trim: true,
    },
    name: {
      type: String,
      required: true,
      trim: true,
    },
    origin: {
      type: String,
      trim: true,
      enum: ["Interno", "Venta"],
    },
    quantity: {
      type: Number,
      required: true,
    },
    presentation: {
      type: String,
      required: true,
    },
    tax: {
      type: Number,
      default: 0.0,
    },
    p_price: {
      type: Number,
      required: true,
    },
    s_price: {
      type: Number,
      required: true,
    },
    category: {
      type: String,
      required: true,
    },
    img: {
      data: Buffer,
      contentType: String,
    },
  },
  {
    timestamps: true,
    versionKey: false,
  }
);

export default model("product", productSchema);
