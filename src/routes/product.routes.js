import { Router } from "express";

import {
  renderProduct,
  createProduct,
  renderProductEdit,
  editProduct,
  deleteProduct,
} from "../Controllers/product.controllers";
import { checkAuth } from "../middleware/auth";
import { checkRoleAuth } from "../middleware/roleAuth";

const router = Router();

router.get("/product/", renderProduct);
router.post("/product/add", createProduct);
router.get("/product/edit/:id", renderProductEdit);
router.put("/product/edit/:id", editProduct);
router.delete("/product/delete/:id", deleteProduct);

//router.get("/user/toogleActive/:id", checkAuth, checkRoleAuth(['user']), toogleUserActive )
//router.get("/user/toogleActive/:id", checkAuth, checkRoleAuth(['user']), toogleUserActive )

export default router;
