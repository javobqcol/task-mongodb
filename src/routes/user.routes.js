//usuarios y roles

import { Router } from "express";

import {  
  renderUser,
  createUser,
  renderUserEdit,
  deleteUser,
  editUser,
  toogleUserActive,
} from "../Controllers/user.controllers";
import { checkAuth } from "../middleware/auth";
import { checkRoleAuth } from "../middleware/roleAuth";

const router = Router();

router.get("/user/", checkAuth, checkRoleAuth(["user"]), renderUser);
router.post("/user/add", createUser);
router.get(
  "/user/edit/:id",
  checkAuth,
  checkRoleAuth(["user"]),
  renderUserEdit
);
router.put(
  "/user/edit/:id",
  checkAuth,
  checkRoleAuth(["user", "admin"]),
  editUser
);
router.delete(
  "/user/delete/:id",
  checkAuth,
  checkRoleAuth(["admin"]),
  deleteUser
);
router.put(
  "/user/toogleActive/:id",
  checkAuth,
  checkRoleAuth(["user"]),
  toogleUserActive
);
export default router;
