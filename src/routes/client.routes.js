import { Router } from "express";

import {
  renderClient,
  createClient,
  renderClientEdit,
  editClient,
  deleteClient,
} from "../Controllers/client.controllers";
import { checkAuth } from "../middleware/auth";
import { checkRoleAuth } from "../middleware/roleAuth";

const router = Router();

router.get("/client/", renderClient);
router.post("/client/add", createClient);
router.get("/client/edit/:id", renderClientEdit);
router.put("/client/edit/:id", editClient);
router.delete("/client/delete/:id", deleteClient);

//router.get("/user/toogleActive/:id", checkAuth, checkRoleAuth(['user']), toogleUserActive )
//router.get("/user/toogleActive/:id", checkAuth, checkRoleAuth(['user']), toogleUserActive )

export default router;
