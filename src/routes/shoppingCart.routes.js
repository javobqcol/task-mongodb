import { Router } from "express";
import {
  renderShoppingCart,
  createShoppingCart,
  editShoppingCart,
  deleteShoppingCart,
  rendershoppingCartEdit,
} from "../Controllers/shoppingCart.controllers";
import { checkAuth } from "../middleware/auth";
import { checkRoleAuth } from "../middleware/roleAuth";

const router = Router();

router.get("/shoppingcart/", renderShoppingCart);
router.post("/shoppingcart/add", createShoppingCart);
router.get("/shoppingcart/edit/:id", rendershoppingCartEdit);
router.put("/shoppingcart/edit/:id", editShoppingCart);
router.delete("/shoppingcart/delete/:id", deleteShoppingCart);

//router.get("/user/toogleActive/:id", checkAuth, checkRoleAuth(['user']), toogleUserActive )
//router.get("/user/toogleActive/:id", checkAuth, checkRoleAuth(['user']), toogleUserActive )

export default router;
